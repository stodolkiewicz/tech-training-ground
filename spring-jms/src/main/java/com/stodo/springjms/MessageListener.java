package com.stodo.springjms;

import com.stodo.springjms.dto.Order;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageListener {

    @JmsListener(destination = "orders")
    public void onMessage(Order order) {
        System.out.println("Otrzymano order: " + order);
    }
}
