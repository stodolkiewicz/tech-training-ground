package com.stodo.springjms;

import com.stodo.springjms.dto.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static java.util.Collections.singletonMap;

@RestController
@RequestMapping("/orders")
public class MessageSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping
    public ResponseEntity create() {
        var orderId = UUID.randomUUID();
        Order order = new Order(orderId.toString(), "order description");

        System.out.println("Sending order " + orderId);
        jmsTemplate.convertAndSend("orders", order);

        return createResponse(orderId);
    }

    private ResponseEntity createResponse(UUID orderId) {
        return ResponseEntity
                .accepted()
                .body(singletonMap("orderId", orderId.toString()));
    }

}
