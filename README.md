todo:
ConfigurationProperties for ResilientTagGetterService (url, timeout)

1.
EXCEPTION HANDLING IN SPRING + BEST PRACTICES. @ControllerAdvice, @ExceptionHandler, CustomException, ResponseEntity

2.
HIBERNATE VALIDATOR WITH CUSTOM ANNOTATIONS TO VALIDATE POST BODY

3.
MAPSTRUCT - https://cupofcodes.pl/mapstruct-czyli-jak-szybko-i-wygodnie-mapowac-obiekty-cz-1/

4.
mockMvc tests from https://reflectoring.io/spring-boot-paging/

5.
MAPSTRUCT should map Post to - > PostWithCommentsDTO (with nested AppUserDTO!)

6.
scrollable resultset https://www.baeldung.com/hibernate-pagination
LOCK MODES

7.
read on ArgumentCaptor

8.
DoInJPA

9.
add gitlab.yml and run unit tests

