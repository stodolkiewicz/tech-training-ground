package com.stodo.springjms.weather.client;

import com.stodo.springjms.weather.client.dto.OpenWeatherWeatherDto;
import com.stodo.springjms.weather.dto.WeatherDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class WeatherClient {
    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/";
    private static final String API_KEY = "62accf76ca3390794e062728e4bd00c7";
    private RestTemplate restTemplate = new RestTemplate();

    public WeatherDto getWeatherForCity(String city) {
        OpenWeatherWeatherDto openWeatherWeatherDto = restTemplate.getForObject(WEATHER_URL + "weather?q={city}&appid={apiKey}&units=metrics",
                OpenWeatherWeatherDto.class, city, API_KEY);

        return WeatherDto.builder()
                .temperature(openWeatherWeatherDto.getMain().getTemp())
                .pressure(openWeatherWeatherDto.getMain().getPressure())
                .humidity(openWeatherWeatherDto.getMain().getHumidity())
                .windSpeed(openWeatherWeatherDto.getWind().getSpeed())
                .build();
    }

}
