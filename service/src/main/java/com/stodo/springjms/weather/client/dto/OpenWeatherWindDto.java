package com.stodo.springjms.weather.client.dto;

import lombok.Getter;

@Getter
public class OpenWeatherWindDto {
    private float speed;
}
