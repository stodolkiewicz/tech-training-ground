package com.stodo.springjms.weather.service;

import com.stodo.springjms.weather.client.WeatherClient;
import com.stodo.springjms.weather.dto.WeatherDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WeatherService {

    private final WeatherClient weatherClient;

    public WeatherDto getWeather() {
        WeatherDto weatherDto = weatherClient.getWeatherForCity("warszawa");
        System.out.println(weatherDto);

        return weatherDto;
    }

}
