package com.stodo.springjms.circuitbreaker.config;

import com.stodo.springjms.circuitbreaker.service.TagGetterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TagGetterServiceConfig {

    @Bean
    public TagGetterService tagGetterService() {
        return new TagGetterService(restTemplate());
    }

    private RestTemplate restTemplate() {
        var factory = new SimpleClientHttpRequestFactory();

        factory.setConnectTimeout(2000);

        return new RestTemplate(factory);
    }
}
