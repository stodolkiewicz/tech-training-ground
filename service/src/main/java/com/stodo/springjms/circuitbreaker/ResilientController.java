package com.stodo.springjms.circuitbreaker;

import com.stodo.springjms.circuitbreaker.dto.TagDto;
import com.stodo.springjms.circuitbreaker.service.ResilientTagGetterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/resilient")
public class ResilientController {

    private final ResilientTagGetterService resilientService;

    @GetMapping
    public List<TagDto> getAllTagsFromPersistence() {
        return resilientService.callPersistenceToGetAllTags();
    }

}
