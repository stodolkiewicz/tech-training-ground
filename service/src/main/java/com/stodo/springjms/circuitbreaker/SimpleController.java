package com.stodo.springjms.circuitbreaker;

import com.stodo.springjms.circuitbreaker.dto.TagDto;
import com.stodo.springjms.circuitbreaker.service.TagGetterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/simple")
public class SimpleController {

    private final TagGetterService tagGetterService;

    @GetMapping
    public List<TagDto> getAllTagsFromPersistence() {
        return tagGetterService.callPersistenceToGetAllTags();
    }

}
