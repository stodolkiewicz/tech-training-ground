package com.stodo.springjms.circuitbreaker.dto;

import lombok.Data;

@Data
public class TagDto {
    Long id;
    String name;
}
