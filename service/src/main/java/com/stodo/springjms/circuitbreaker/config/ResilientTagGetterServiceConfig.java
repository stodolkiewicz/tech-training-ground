package com.stodo.springjms.circuitbreaker.config;

import com.stodo.springjms.circuitbreaker.service.ResilientTagGetterService;
import com.stodo.springjms.circuitbreaker.service.TagGetterService;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class ResilientTagGetterServiceConfig {

    @Bean
    public ResilientTagGetterService resilientTagGetterService(TagGetterService tagGetterService) {
        return new ResilientTagGetterService(tagGetterService, retry(), circuitBreaker());
    }

    private Retry retry() {
        Retry retry = Retry.of("get all tags", RetryConfig.custom()
                .maxAttempts(10)
                .waitDuration(Duration.ofMillis(200))
                .build());

        retry.getEventPublisher()
                .onEvent(event -> System.out.println("Retry event: " + event));

        return retry;
    }

    private CircuitBreaker circuitBreaker() {
        CircuitBreaker circuitBreaker = CircuitBreaker.of("getAllTags", CircuitBreakerConfig.custom()
                .failureRateThreshold(50)
                .minimumNumberOfCalls(3)
                .slidingWindowSize(3)
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                .waitDurationInOpenState(Duration.ofSeconds(5))
                .permittedNumberOfCallsInHalfOpenState(1)
                .build());

        circuitBreaker.getEventPublisher()
                .onEvent(System.out::println);

        return circuitBreaker;
    }

}
