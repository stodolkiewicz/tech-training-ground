package com.stodo.springjms.circuitbreaker.service;

import com.stodo.springjms.circuitbreaker.dto.TagDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public class TagGetterService {

    private final RestTemplate restTemplate;

    public List<TagDto> callPersistenceToGetAllTags() {

        ResponseEntity<TagDto[]> response = restTemplate.getForEntity("http://localhost:8080/tag", TagDto[].class);
        List<TagDto> tagDtos = response.getBody() == null ? new ArrayList<>() : Arrays.asList(response.getBody());

        return tagDtos;
    }
}
