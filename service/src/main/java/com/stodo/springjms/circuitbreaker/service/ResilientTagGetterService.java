package com.stodo.springjms.circuitbreaker.service;

import com.stodo.springjms.circuitbreaker.dto.TagDto;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.retry.Retry;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public class ResilientTagGetterService {

    private final TagGetterService tagGetterService;
    private final Retry retry;
    private final CircuitBreaker circuitBreaker;

    public List<TagDto> callPersistenceToGetAllTags() {
        return Decorators.ofSupplier( () -> tagGetterService.callPersistenceToGetAllTags())
                .withRetry(retry)
                .withCircuitBreaker(circuitBreaker)
                .withFallback(e -> Collections.emptyList())
                .get();
    }
}
