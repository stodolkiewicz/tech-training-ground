docker exec -it kafka_broker1_1 kafka-topics --zookeeper zookeeper:2181 --create
--topic softwareskill_safe_topic --partitions 3 --replication-factor 3
--config unclean.leader.election.enable=false --config min.insync.replicas=2