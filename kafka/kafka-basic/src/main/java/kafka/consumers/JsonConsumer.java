package kafka.consumers;

import kafka.producers.jsonserializer.Person;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

@Slf4j
public class JsonConsumer {
    static final String TOPIC_NAME = "persons";
    static final String BOOTSTRAP_SERVERS = "localhost:9092";
    static final String GROUP_ID = "group-1";

    public static void main(String[] args) {
        // 1. Consumer properties
        Properties consumerProps = new Properties();
        consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);

        // 2. Create Kafka Consumer
        KafkaConsumer<String, Person> consumer = new KafkaConsumer<>(
                consumerProps,
                new StringDeserializer(),
                new JsonDeserializer<Person>(Person.class));

        // 3. Subscribe to topic
        consumer.subscribe(Collections.singletonList(TOPIC_NAME));

        // 4. Poll records
        try {
            while (true) {
                ConsumerRecords<String, Person> records = consumer.poll(Duration.ofSeconds(10));
                for (ConsumerRecord<String, Person> record : records) {
                    final var person = record.value();
                    log.info("firstname {}, lastName {}, birthday {}",
                            person.getFirstName(), person.getLastName(), person.getBirthday());
                }
            }
        } catch (Exception e) {
            log.error("Exception while polling messages: {}", e.toString());
        } finally {
            consumer.close();
        }
    }

}
