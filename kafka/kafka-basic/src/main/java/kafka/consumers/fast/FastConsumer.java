package kafka.consumers.fast;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Properties;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FastConsumer {

    static final String TOPIC_NAME = "softwareskill_safe_topic";
    static final String GROUP_ID = "group-1";
    static final String BOOTSTRAP_SERVERS = "127.0.0.1:9092";

    public static void main(String[] args) {
        Properties consumerProperties = createConsumerProperties();

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProperties);
        Thread thread = new Thread(new FastConsumerRunnable(consumer, TOPIC_NAME));

        thread.start();
    }

    private static Properties createConsumerProperties() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true"); // default
        properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 5000); //default
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        properties.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, 2048);
        properties.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, 1000);

        return properties;
    }


}
