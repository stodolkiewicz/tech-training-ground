package kafka.producers;

import kafka.producers.jsonserializer.KafkaJsonSerializer;
import kafka.producers.jsonserializer.Person;
import kafka.producers.jsonserializer.PersonFactory;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class JsonProducer {
    public static void main(String[] args) {
        final var TOPIC_NAME = "persons";
        final var BOOTSTRAP_SERVERS = "localhost:9092";

        // create producer properties
        Properties kafkaProps = new Properties();
        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaJsonSerializer.class.getName());

        // create producer
        KafkaProducer<String, Person> producer = new KafkaProducer<>(kafkaProps);


        for(int i = 0; i < 10; i++) {
            // create records
            Person randomPerson = PersonFactory.createRandomPerson();
            ProducerRecord<String, Person> record = new ProducerRecord<>(TOPIC_NAME, createKey(randomPerson), randomPerson);

            // send data
            producer.send(record);
        }

        // clear connection
        producer.flush();
        producer.close();
    }

    private static String createKey(Person person) {
        return person.getFirstName() + person.getLastName();
    }
}
