package kafka.producers.jsonserializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;

@Slf4j
public class KafkaJsonSerializer implements Serializer {

    @Override
    public byte[] serialize(String s, Object o) {
        byte[] returnValue = null;
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            returnValue = objectMapper.writeValueAsBytes(o);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }

        return returnValue;
    }
}
