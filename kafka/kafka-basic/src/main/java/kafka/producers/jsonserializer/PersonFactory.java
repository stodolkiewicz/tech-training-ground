package kafka.producers.jsonserializer;

import com.github.javafaker.Faker;

public class PersonFactory {

    public static Person createRandomPerson() {
        Faker faker = new Faker();
        return Person.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .birthday(faker.date().birthday())
                .build();
    }
}