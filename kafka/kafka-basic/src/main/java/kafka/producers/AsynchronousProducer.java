package kafka.producers;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Objects;
import java.util.Properties;

@Slf4j
public class AsynchronousProducer {

    public static void main(String[] args) {
        final var TOPIC_NAME = "softwareskilltopic";
        final var BOOTSTRAP_SERVERS = "localhost:9092";

        // create producer properties
        Properties kafkaProps = new Properties();
        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<>(kafkaProps);

        // create a ProducerRecord
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, "key", "simple asynchronous producer");

        // send data
        producer.send(record, new ProducerCallback());

        // clear connection
        producer.flush();
        producer.close();
    }

    private static class ProducerCallback implements Callback {

        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if(Objects.nonNull(e)) {
                log.error("Error while asynchronous send", e);
            } else {
                log.info("RecordMetadata after synchronous send: topic={}, partition={}, offset={}",
                        recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset());

            }
        }
    }

}
