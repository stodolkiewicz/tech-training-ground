package kafka.producers;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Duration;
import java.time.Instant;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
public class SafeProducer {
    public static void main(String[] args) {

        final String TOPIC_NAME = "softwareskill_safe_topic";
        final String BOOTSTRAP_SERVERS = "localhost:9092";

        // 1. Create producer properties
        Properties kafkaProps = new Properties();
        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // performance settings
        kafkaProps.put(ProducerConfig.LINGER_MS_CONFIG, 5);
        kafkaProps.put(ProducerConfig.BATCH_SIZE_CONFIG, 32768);
        kafkaProps.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "lz4");
        kafkaProps.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        kafkaProps.put(ProducerConfig.ACKS_CONFIG, "all");

        // safe settings
        kafkaProps.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);
        kafkaProps.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true"); // also set acks to all

        // 2. create producer
        KafkaProducer<String, String> producer = new KafkaProducer<>(kafkaProps);
        Instant start = Instant.now();

        for(int i = 0; i < 500; i++) {
            try {
                // 3. create producer records
                ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, "key", "safe producer " + i);

                // 4. send data synchronously
                Future<RecordMetadata> future = producer.send(record);
                producer.flush();

                RecordMetadata recordMetadata = future.get();

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("Sending interrupted", e);
            } catch (Exception e) {
                log.error("Error while synchronous send", e);
            }
        }

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        log.info("Time elapsed: {}", timeElapsed);
        log.info("send/s: {}", 10000000 / timeElapsed * 1000);
    }

}
