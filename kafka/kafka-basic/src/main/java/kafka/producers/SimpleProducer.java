package kafka.producers;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class SimpleProducer {
    public static void main(String[] args) {
        final var TOPIC_NAME = "softwareskilltopic";
        final var BOOTSTRAP_SERVERS = "localhost:9092";

        // create producer properties
        Properties kafkaProps = new Properties();
        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<>(kafkaProps);

        // create a ProducerRecord
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, "key", "simple fire and forget  procedure");

        // send data
        producer.send(record);

        // clear connection
        producer.flush();
        producer.close();
    }
}
