package com.hwmonitor.producer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Data
public class HardwareInfo {
    Long availableMemory;
    Long discFreeSpace;

    public HardwareInfo(@JsonProperty("availableMemory") Long availableMemory,
                        @JsonProperty("discFreeSpace") Long discFreeSpace) {
        this.availableMemory = availableMemory;
        this.discFreeSpace = discFreeSpace;
    }
}
