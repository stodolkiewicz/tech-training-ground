package com.hwmonitor.producer;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.WakeupException;

import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class KafkaProducerRunnable implements Runnable {
    AtomicBoolean closed = new AtomicBoolean(false);
    String topicName;
    KafkaProducer producer;

    public KafkaProducerRunnable(KafkaProducer producer, String topicName) {
        this.topicName = topicName;
        this.producer = producer;
    }

    @Override
    public void run() {

        while (!closed.get()) {
            try {
                while(!closed.get()) {
                    HardwareInfo hardwareInfo = HardwareInfoFactory.createHardwareInfo();
                    ProducerRecord<String, HardwareInfo> record = new ProducerRecord<>(topicName, "key", hardwareInfo);

                    // send data
                    producer.send(record);
                    producer.flush();
                    Thread.sleep(2000);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error("Sending interrupted", e);
            } catch (WakeupException e) {
                if(!closed.get()) throw e;
            } finally {
                producer.close();
            }
        }
    }
}
