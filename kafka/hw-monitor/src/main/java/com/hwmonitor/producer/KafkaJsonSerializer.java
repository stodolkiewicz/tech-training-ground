package com.hwmonitor.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;

@Slf4j
public class KafkaJsonSerializer implements Serializer {
    @Override
    public byte[] serialize(String topic, Object o) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            retVal = objectMapper.writeValueAsBytes(o);
        } catch (JsonProcessingException e) {
            log.error("Error while serializing message", e);
        }

        return retVal;
    }
}
