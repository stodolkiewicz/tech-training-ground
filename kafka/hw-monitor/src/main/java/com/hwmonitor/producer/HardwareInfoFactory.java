package com.hwmonitor.producer;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

import java.io.File;

public class HardwareInfoFactory {

    public static HardwareInfo createHardwareInfo() {
        HardwareAbstractionLayer hardwareAbstractionLayer = new SystemInfo().getHardware();
        File mainDrive = new File("/");

        return HardwareInfo.builder()
                .availableMemory(hardwareAbstractionLayer.getMemory().getAvailable())
                .discFreeSpace(mainDrive.getFreeSpace())
                .build();
    }

}
