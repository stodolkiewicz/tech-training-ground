package com.hwmonitor.consumer;

import com.hwmonitor.producer.HardwareInfo;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class KafkaConsumerRunnable implements Runnable {
    AtomicBoolean closed = new AtomicBoolean(false);
    KafkaConsumer consumer;
    String topicName;

    public KafkaConsumerRunnable(KafkaConsumer consumer, String topicName) {
        this.consumer = consumer;
        this.topicName = topicName;
    }

    @Override
    public void run() {
        try {
            consumer.subscribe(Arrays.asList(topicName));
            while (!closed.get()) {
                ConsumerRecords<String, HardwareInfo> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, HardwareInfo> record : records) {
                    log.info("Client hardware info: " + LocalDateTime.now() + " " + record.value().toString());
                }
            }
        } catch (WakeupException e) {
            if(!closed.get()) throw e;
        } finally {
            consumer.close();
        }
    }

    public void shutdown() {
        closed.set(true);
        consumer.wakeup();
    }

/*

    wakeup() is the only consumer method that is safe to call from a different thread.
    Calling wakeup will cause poll() to exit with WakeupException ,
    or if consumer. wakeup() was called while the thread was not waiting on poll,
    the exception will be thrown on the next iteration when poll() is called.

*/
}
