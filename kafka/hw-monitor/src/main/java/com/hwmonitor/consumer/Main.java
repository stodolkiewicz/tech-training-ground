package com.hwmonitor.consumer;

import com.hwmonitor.producer.HardwareInfo;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Properties;

public class Main {

    private static final String TOPIC_NAME = "cpuinfo";
    private static final String GROUP_ID = "group-1";
    private static final String BOOTSTRAP_SERVERS = "127.0.0.1:9092";

    public static void main(String[] args) {
        Properties consumerProperties = createConsumerProperties();
        KafkaConsumer<String, HardwareInfo> consumer = new KafkaConsumer<>(
                consumerProperties,
                new StringDeserializer(),
                new KafkaJsonDeserializer<HardwareInfo>(HardwareInfo.class));

        Thread thread = new Thread(new KafkaConsumerRunnable(consumer, TOPIC_NAME));
        thread.start();
    }

    private static Properties createConsumerProperties() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG ,BOOTSTRAP_SERVERS);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG , GROUP_ID);

        return properties;
    }
}
