package com.hwmonitor.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

@Slf4j
public class KafkaJsonDeserializer<T> implements Deserializer {

    private Class<T> type;

    public KafkaJsonDeserializer(Class<T> type) {
        this.type = type;
    }

    @Override
    public T deserialize(String topic, byte[] bytes) {
        ObjectMapper mapper = new ObjectMapper();

        T returnValue = null;
        try {
            returnValue = mapper.readValue(bytes, type);
        } catch (IOException e) {
            log.error("Error while deserializing");
        }

        return returnValue;
    }
}
