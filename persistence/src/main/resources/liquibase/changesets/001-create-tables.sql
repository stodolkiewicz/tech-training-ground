--liquibase formatted sql

--changeset stodo:create-post-table
CREATE TABLE POST (
    ID BIGINT NOT NULL,
    TITLE VARCHAR,
    TEXT VARCHAR,
    PRIMARY KEY(ID)
)
--rollback DROP TABLE POST

--changeset stodo:create-post-table-sequence
CREATE SEQUENCE post_id_seq increment by 10 start with 10;
--rollback DROP SEQUENCE post_id_seq

--changeset stodo:create-post_details-table
CREATE TABLE POST_DETAILS (
    CREATED_ON TIMESTAMP,
    CREATED_BY VARCHAR(255),
    POST_ID BIGINT,
    PRIMARY KEY(POST_ID),
    FOREIGN KEY(POST_ID) REFERENCES POST(ID)
)
--rollback DROP TABLE POST_DETAILS;

--changeset stodo:create-user-table
CREATE TABLE APP_USER (
    ID BIGSERIAL NOT NULL,
    NAME VARCHAR
)
--rollback DROP TABLE POST_COMMENT;

--changeset stodo:create-user-table-sequence
CREATE SEQUENCE app_user_id_sequence increment by 1 start with 1;
--rollback DROP SEQUENCE app_user_id_sequence

--changeset stodo:create-post_comment-table
CREATE TABLE POST_COMMENT (
    ID BIGSERIAL NOT NULL,
    REVIEW VARCHAR,
    POST_ID BIGINT,
    APP_USER_ID BIGINT,
    PRIMARY KEY(ID),
    FOREIGN KEY(POST_ID) REFERENCES POST(ID)
)
--rollback DROP TABLE POST_COMMENT;

--changeset stodo:create-tag-table
CREATE TABLE TAG (
    ID BIGSERIAL NOT NULL,
    NAME VARCHAR,
    PRIMARY KEY(ID)
)
--rollback DROP TABLE TAG

--changeset stodo:create-post_tag-table
CREATE TABLE POST_TAG (
    POST_ID BIGSERIAL NOT NULL,
    TAG_ID BIGSERIAL NOT NULL,
    FOREIGN KEY(POST_ID) REFERENCES POST(ID),
    FOREIGN KEY(TAG_ID) REFERENCES TAG(ID),
    PRIMARY KEY(POST_ID, TAG_ID)
)
--rollback DROP TABLE POST_TAG