### How to
**To start postgres on docker**  
`cd docker`  
`docker-compose up`

**Start the application**  
    `mvn spring-boot:run -Dspring-boot.run.profiles=postgres`

**Kill processes by port number***  
    `netstat -ano | findstr :<yourPortNumber>`  
    `taskkill /PID <typeyourPIDhere> /F`

**To trigger livereload**  
    `ctrl + f9`
-----------------------------
### Data Model

![Data Model](../../../data-model.png "Data Model")

### Datasource & Logging

logging guide:  
https://thorben-janssen.com/hibernate-logging-guide/#Log4J2

dataSourceProxy documentation:  
http://ttddyy.github.io/datasource-proxy/docs/current/user-guide/#query-logging-listener

-----------------------------

### Testcontainers

https://blog.sandra-parsick.de/2020/05/21/using-testcontainers-in-spring-boot-tests-for-database-integration-tests/  
https://www.testcontainers.org/test_framework_integration/manual_lifecycle_control/#singleton-containers

@Transactional in Integration tests - db changes, made in a given test, will be rolled back. Which is awesome :) 

------------------------------

### Batching

hibernate.jdbc.batch_size - turns on batching, sets default batch size

hibernate.order_updates - sort queries working on same entities
hibernate.order_inserts - sort queries working on same entities

batch can only operate on ONE table!

hibernate turns off batching for IDENTITY id generation strategy

------------------------------

### Caching
**What is caching?**  
Technique enabling faster execution of operations 
by saving the results in memory for some time.

**Example**:  
reading all rows in some configuration table can be cached and the results can be reused

As a cache can be used:
- RAM
- Redis
- files

Hibernate Caches  

**First** Level Cache - on, by default. On the Session level.  

**Second** Level Cache - off, by default. On the SessionFactory level.
hibernate.cache.use_query_cache - to turn it on

**Query cache** TODO

There also is Spring Cache - TODO

- You need a provider like Ehcache 3  
- Entity can be cached when it has @Cacheable/@Cache annotation


EhCache xml configuration:  https://www.ehcache.org/documentation/3.1/107.html

###Paging and Sorting
https://reflectoring.io/spring-boot-paging/
https://www.youtube.com/watch?v=B-IHz3oVUG8

### MapStruct
https://mapstruct.org/documentation/stable/reference/html/#retrieving-mapper  
https://cupofcodes.pl/mapstruct-czyli-jak-szybko-i-wygodnie-mapowac-obiekty-cz-1/

### Hibernate Core

###### Detach Entity from PersistenceContext  
https://www.youtube.com/watch?v=p-euc3vQQAc  

###### Entity Graph
https://www.baeldung.com/jpa-entity-graph