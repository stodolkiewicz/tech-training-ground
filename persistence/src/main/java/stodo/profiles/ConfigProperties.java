package stodo.profiles;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "first")
public class ConfigProperties {
    private String property;
    private String otherProperty;
}