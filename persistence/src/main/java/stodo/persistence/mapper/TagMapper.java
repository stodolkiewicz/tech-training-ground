package stodo.persistence.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import stodo.persistence.dto.TagDTO;
import stodo.persistence.model.Tag;

@Mapper
public interface TagMapper {
    TagMapper INSTANCE = Mappers.getMapper( TagMapper.class );

    TagDTO mapToTagDTO(Tag tag);
}
