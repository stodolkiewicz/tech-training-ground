package stodo.persistence.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import stodo.persistence.dto.TagDTO;
import stodo.persistence.model.Tag;
import stodo.persistence.service.TagService;

import java.util.List;

@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    TagService tagService;

    @GetMapping
    public List<TagDTO> getAll() {

        return tagService.findAllTags();

    }

}
