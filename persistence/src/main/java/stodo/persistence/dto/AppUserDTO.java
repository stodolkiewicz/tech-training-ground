package stodo.persistence.dto;

import lombok.Data;

@Data
public class AppUserDTO {
    private Long id;
    private String name;
}
