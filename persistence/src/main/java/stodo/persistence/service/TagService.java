package stodo.persistence.service;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.dto.TagDTO;
import stodo.persistence.mapper.PostMapper;
import stodo.persistence.mapper.TagMapper;
import stodo.persistence.model.Post;
import stodo.persistence.model.Tag;
import stodo.persistence.repository.TagRepository;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    public List<TagDTO> findAllTags() {
        List<Tag> tags = tagRepository.findAll();

        TagMapper mapper = Mappers.getMapper(TagMapper.class);
        List<TagDTO> tagDTOs = tags.stream()
                .map(mapper::mapToTagDTO)
                .collect(toList());

        return tagDTOs;
    }

}
